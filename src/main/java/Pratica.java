
import utfpr.ct.dainf.pratica.PontoXY;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Pratica {

    public static void main(String[] args) {
        PontoXZ p = new PontoXZ(-3,2);
        PontoXY p1 = new PontoXY(0,2);
        System.out.println(String.format("Distancia = %f", p.dist(p1)));
        
    }
    
}
