package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;
    public Ponto() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    
    public Ponto(double _x, double _y, double _z) {
        this.x = _x;
        this.y = _y;
        this.z = _z;
    }
    
    public double dist(Ponto p) {
        return Math.sqrt(Math.pow(p.getX()-this.getX(), 2) +
                         Math.pow(p.getY()-this.getY(), 2) +
                         Math.pow(p.getZ()-this.getZ(), 2));
    }

    
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }
    
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    
    @Override
    public String toString() {
        return String.format("%s(%f,%f,%f)", this.getNome(), this.getX(), this.getY(), this.getZ());
    }
    
        
    @Override
    public boolean equals(Object o) {
        if(o != null && o instanceof Ponto)
        {
            Ponto p = (Ponto) o;
            return this.x == p.getX() && this.y == p.getY() && this.z == p.getZ();
        }
        return false;
        
    }
    
    

}
