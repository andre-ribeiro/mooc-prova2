/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1796283
 */
public class PontoXY extends Ponto2D {
    public PontoXY() {
        super(0,0,0);
    }
    
    public PontoXY(double _x, double _y) {
        super(_x, _y, 0);
    }
    
    @Override
    public String toString() {
        return String.format("%s(%f,%f)", this.getNome(), this.getX(), this.getY());
    }
    
}
