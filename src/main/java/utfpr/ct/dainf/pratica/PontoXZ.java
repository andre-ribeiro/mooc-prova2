/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1796283
 */
public class PontoXZ extends Ponto2D {
    public PontoXZ() {
        super(0,0,0);
    }
    
    public PontoXZ(double _x, double _z) {
        super(_x, 0, _z);
    }
    
    @Override
    public String toString() {
        return String.format("%s(%f,%f)", this.getNome(), this.getX(), this.getZ());
    }
}
