/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1796283
 */
public class PontoYZ extends Ponto2D {
    public PontoYZ() {
        super(0,0,0);
    }
    public PontoYZ(double _y, double _z) {
        super(0, _y, _z);
    }
    
    @Override
    public String toString() {
        return String.format("%s(%f,%f)", this.getNome(), this.getY(), this.getZ());
    }
}
